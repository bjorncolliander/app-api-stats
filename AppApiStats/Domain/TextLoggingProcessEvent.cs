﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TextLoggingProcessEvent.cs" company="Bosbec AB">
//   Copyright © Bosbec AB
// </copyright>
// <summary>
//   Defines the TextLoggingProcessEvent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bosbec.MobileAction.Domain.Processes.LogEvents
{
    using System;
    using System.Collections.Generic;

    using Newtonsoft.Json;

    /// <summary>
    /// The text logging process event.
    /// </summary>
    [Serializable]
    public class TextLoggingProcessEvent : ProcessLogEvent
    {
        [JsonConstructor]
        protected TextLoggingProcessEvent() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextLoggingProcessEvent"/> class.
        /// </summary>
        /// <param name="processId">
        /// The process id.
        /// </param>
        public TextLoggingProcessEvent(Guid processId) : base(processId)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextLoggingProcessEvent"/> class.
        /// </summary>
        /// <param name="processId">
        /// The process id.
        /// </param>
        /// <param name="customLogDataValue">
        /// The custom log data value.
        /// </param>
        /// <param name="customKey">
        /// The custom key.
        /// </param>
        public static TextLoggingProcessEvent Create(Guid processId, string customLogDataValue, string customKey = "custom-log-data")
        {
            var evnt = new TextLoggingProcessEvent(processId);
            evnt.CustomLogData(customLogDataValue, customKey);

            return evnt;
        }

        /// <summary>
        /// The custom log data.
        /// </summary>
        /// <param name="customLogDataValue">
        /// The custom log data value.
        /// </param>
        /// <param name="customKey">
        /// The custom key.
        /// </param>
        public void CustomLogData(string customLogDataValue, string customKey = "custom-log-data")
        {
            if (LogEvents.ContainsKey(customKey))
            {
                LogEvents[customKey] += customLogDataValue;
            }
            else
            {
                LogEvents.Add(customKey, customLogDataValue);
            }
        }

        /// <summary>
        /// The custom log data tmp.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <param name="customKey">
        /// The custom key.
        /// </param>
        public void CustomLogDataTmp(object obj, string customKey = "custom-log-data")
        {
            var val = obj != null ? obj.ToString() : "-";
            if (LogEvents.ContainsKey(customKey))
            {
                LogEvents[customKey] += val;
            }
            else
            {
                LogEvents.Add(customKey, val);
            }
        }
    }
}