﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IProcessLogEvent.cs" company="Bosbec AB">
//   Copyright © Bosbec AB
// </copyright>
// <summary>
//   The ProcessLogEvent interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bosbec.MobileAction.Domain.Processes.LogEvents
{
    using System.Collections.Generic;

    
    /// <summary>
    /// The ProcessLogEvent interface.
    /// </summary>
    public interface IProcessLogEvent
    {
        /// <summary>
        /// Gets the log events.
        /// </summary>
        Dictionary<string, string> LogEvents { get; }
    }
}