﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProcessLogEvent.cs" company="Bosbec AB">
//   Copyright © Bosbec AB
// </copyright>
// <summary>
//   Defines the ProcessLogEvent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Bosbec.MobileAction.Domain.Processes.LogEvents
{
    using System;
    using System.Collections.Generic;

    using Newtonsoft.Json;

    /// <summary>
    /// The process log event.
    /// </summary>
    [Serializable]
    public abstract class ProcessLogEvent : IProcessLogEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessLogEvent"/> class.
        /// </summary>
        // TODO: do we need this ? 
        [JsonConstructor]
        protected ProcessLogEvent()
        {
            this.LogEvents = this.LogEvents ?? new Dictionary<string, string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessLogEvent"/> class.
        /// </summary>
        /// <param name="processId">
        /// The process id.
        /// </param>
        protected ProcessLogEvent(Guid processId)
        {
            this.LogEvents = this.LogEvents ?? new Dictionary<string, string>();
            this.Id = Guid.NewGuid();
            this.CreatedOn = DateTime.Now;
            this.ProcessId = processId;
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets the created on.
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the process id.
        /// </summary>
        public Guid ProcessId { get; set; }

        /// <summary>
        /// Gets the log events.
        /// </summary>
        public Dictionary<string, string> LogEvents { get; set; }
    }
}