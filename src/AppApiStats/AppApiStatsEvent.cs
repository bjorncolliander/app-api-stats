﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppApiStats
{
    public class AppApiStatsEvent
    {
        public string DeviceType { get; set; }

        public string AppId { get; set; }

        public string AppVersion { get; set; }

        public string AuthenticationToken { get; set; }

        public string UserId { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
