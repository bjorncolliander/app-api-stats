﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bosbec.MobileAction.Domain.Processes.LogEvents;
using Bosbec.ServiceBus;
using Bosbec.ServiceBus.Pipeline.IncomingSteps.Handlers;

namespace AppApiStats
{
    class TextLoggingProcessEventHandler : IMessageHandler<TextLoggingProcessEvent>
    {
        private readonly IPublisher _publisher;

        public TextLoggingProcessEventHandler(IPublisher publisher)
        {
            _publisher = publisher;
        }

        public void Handle(TextLoggingProcessEvent message)
        {
            if (message.LogEvents.Any())
            {
                if (message.LogEvents.ContainsKey("appId"))
                {
                    var apiEvent = new AppApiStatsEvent
                    {
                        AppId = message.LogEvents["appId"],
                        AppVersion = message.LogEvents["appVersion"],
                        DeviceType = message.LogEvents["deviceType"],
                        AuthenticationToken = message.LogEvents["AuthenticationToken"],
                        UserId = message.LogEvents["UserId"],
                        CreatedOn = message.CreatedOn
                    };

                    Console.WriteLine("----------- AppApiEvent ---------------");
                    Console.WriteLine("AppId: "+ apiEvent.AppId);
                    Console.WriteLine("AppVersion: " + apiEvent.AppVersion);
                    Console.WriteLine("DeviceType: " + apiEvent.DeviceType);
                    Console.WriteLine("CreatedOn: " + apiEvent.CreatedOn);

                    _publisher.Send(apiEvent, null, new Dictionary<string, string>()
                    {
                        {
                            "send-time", message.CreatedOn.ToString("O")
                        }
                    });

                }
            }
        }
    }
}
