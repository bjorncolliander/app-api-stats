﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Bosbec.ServiceBus.Hosting;
using Bosbec.ServiceBus.Hosting.Configuration;
using Bosbec.ServiceBus.Hosting.Configuration.DefaultConfigurationConfigurer;
using Bosbec.ServiceBus.RabbitMq;
using RabbitMQ.Client;

namespace AppApiStats
{
    class Program
    {

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            var wh = new AutoResetEvent(false);

            Console.WriteLine("Press ctrl+c to stop");

            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                Console.WriteLine("Stopping everything (ctrl-key-press)!");
                
                wh.Set();

                Console.WriteLine("Press enter to quit");
                Console.ReadLine();
            };

            var config = Setup();

            using (var host = new ServiceBusHost(config))
            {
                host.Start();

                // Here we send a message that will be handled in the MessageHandler defined above.
                //host.Send(new Message { Text = "Some text" });

                wh.WaitOne();
            }


        }

        /// <summary>
        /// The setup.
        /// </summary>
        private static IConfiguration Setup()
        {
            var rabbitConfig = new RabbitMqConfiguration
            {
                Uri = new System.Uri("amqp://mr-bunny:8YCKh3cBan-nAmk@rabbit1.aws.mobileresponse.se:5672"),
                QueueName = "AppApiStats",
                QueueExclusive = true,
                ApplicationName = "AppApiStats",
                ApplicationInformation = "Logging information about appId, appVersion and deviceType"
            };

            return Configure.UsingDefaultContainer().WithDefaultTransportUsingConfiguration(rabbitConfig)
                .GetConfiguration();
        }

        

       
       
    }
}
